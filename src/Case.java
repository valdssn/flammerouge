
/**
 * Classe Case
 *
 */
public class Case {

	/**
	 * Attribut de type Cycliste
	 */
	private Cycliste c;

	/**
	 * Constructeur de la classe Case sans parametre
	 */
	public Case() {
		this.c = null;
	}

	/**
	 * Methode estVide
	 * 
	 * @return true si la case est vide
	 */
	public boolean estVide() {
		if (this.c != null) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Methode getCycliste
	 * 
	 * @return le cycliste present sur la case
	 */
	public Cycliste getCycliste() {
		return this.c;
	}

	/**
	 * Methode setCycliste qui permet de placer un cycliste sur une case
	 * 
	 * @param cy
	 *            objet de type cycliste
	 */
	public void setCycliste(Cycliste cy) {
		this.c = cy;
	}

	/**
	 * Methode viderCase qui permet d'enlever le cycliste present sur la case
	 */
	public void viderCase() {
		this.c = null;
	}

}
