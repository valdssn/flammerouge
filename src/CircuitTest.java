import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CircuitTest {
	
	@Test
	/**
	 * test du constructeur Circuit
	 */
	void test_constructeur_Circuit() {
		// le txt Test contient la ligne suivante
		//0123
		//Preparation des donnees 
		Circuit c=new Circuit("Test");
		//Validation du resultat
		assertEquals("La cellule devrait etre normale de type 0", 0, c.getCircuit().get(0).getTypeCase());
		assertEquals("La cellule devrait etre une montee de type 1", 1, c.getCircuit().get(1).getTypeCase());
		assertEquals("La cellule devrait etre une descente de type 2", 2, c.getCircuit().get(2).getTypeCase());
		assertEquals("La cellule devrait etre une arrivee de type 3", 3, c.getCircuit().get(3).getTypeCase());
	}
	
	@Test
	/**
	 * test de la methode toString quand le circuit est vide
	 */
	void test_toString() {
		//Preparation des donnees 
		Circuit c=new Circuit("Test");
		//Validation du resultat
		assertEquals("La cellule devrait etre normale de type 0", "N                 |    |    | \n"+"M                  \\    \\    \\ \n"+"D                 /    /    / \n"+"A                 |    |    | \n", c.toString());
	}

	@Test
	/**
	 * test de la methode toString quand il y a un rouleur rouge sur la premiere cellule
	 */
	void test_toString2() {
		//Preparation des donnees 
		Circuit c=new Circuit("Test");
		Rouleur r=new Rouleur("R");
		r.avancer(c.getCircuit().get(0));
		//Validation du resultat
		assertEquals("La cellule devrait etre normale de type 0", "N                 |    | RR | \n"+"M                  \\    \\    \\ \n"+"D                 /    /    / \n"+"A                 |    |    | \n", c.toString());
	}

	@Test
	/**
	 * test de la methode toString quand il y a un rouleur rouge et un sprinteur rouge sur la premiere cellule
	 */
	void test_toString3() {
		//Preparation des donnees 
		Circuit c=new Circuit("Test");
		Rouleur r=new Rouleur("R");
		Sprinteur s=new Sprinteur("R");
		r.avancer(c.getCircuit().get(0));
		s.avancer(c.getCircuit().get(0));
		//Validation du resultat
		assertEquals("La cellule devrait etre normale de type 0", "N                 | SR | RR | \n"+"M                  \\    \\    \\ \n"+"D                 /    /    / \n"+"A                 |    |    | \n", c.toString());
	}

	@Test
	/**
	 * test de la methode toString quand il y a un rouleur rouge sur la premiere cellule et un sprinteur rouge sur la montee
	 */
	void test_toString4() {
		//Preparation des donnees 
		Circuit c=new Circuit("Test");
		Rouleur r=new Rouleur("R");
		Sprinteur s=new Sprinteur("R");
		s.avancer(c.getCircuit().get(1));
		r.avancer(c.getCircuit().get(0));
		//Validation du resultat
		assertEquals("La cellule devrait etre normale de type 0", "N                 |    | RR | \n"+"M                  \\    \\ SR \\ \n"+"D                 /    /    / \n"+"A                 |    |    | \n", c.toString());
	}
	
	@Test
	/**
	 * test de la methode toString quand il y a un rouleur rouge et un sprinteur rouge sur la montee
	 */
	void test_toString5() {
		//Preparation des donnees 
		Circuit c=new Circuit("Test");
		Rouleur r=new Rouleur("R");
		Sprinteur s=new Sprinteur("R");
		r.avancer(c.getCircuit().get(1));
		s.avancer(c.getCircuit().get(1));
		//Validation du resultat
		assertEquals("La cellule devrait etre normale de type 0", "N                 |    |    | \n"+"M                  \\ SR \\ RR \\ \n"+"D                 /    /    / \n"+"A                 |    |    | \n", c.toString());
	}
	
	@Test
	/**
	 * test de la methode toString quand il y a un rouleur rouge sur la montee et un sprinteur rouge sur la descente
	 */
	void test_toString6() {
		//Preparation des donnees 
		Circuit c=new Circuit("Test");
		Rouleur r=new Rouleur("R");
		Sprinteur s=new Sprinteur("R");
		r.avancer(c.getCircuit().get(1));
		s.avancer(c.getCircuit().get(2));
		//Validation du resultat
		assertEquals("La cellule devrait etre normale de type 0", "N                 |    |    | \n"+"M                  \\    \\ RR \\ \n"+"D                 /    / SR / \n"+"A                 |    |    | \n", c.toString());
	}

	@Test
	/**
	 * test de la methode toString quand il y a un rouleur rouge et un sprinteur rouge sur la descente
	 */
	void test_toString7() {
		//Preparation des donnees 
		Circuit c=new Circuit("Test");
		Rouleur r=new Rouleur("R");
		Sprinteur s=new Sprinteur("R");
		r.avancer(c.getCircuit().get(2));
		s.avancer(c.getCircuit().get(2));
		//Validation du resultat
		assertEquals("La cellule devrait etre normale de type 0", "N                 |    |    | \n"+"M                  \\    \\    \\ \n"+"D                 / SR / RR / \n"+"A                 |    |    | \n", c.toString());
	}	

	@Test
	/**
	 * test de la methode toString quand il y a un rouleur rouge sur la descente et un sprinteur rouge sur l arrivee
	 */
	void test_toString8() {
		//Preparation des donnees 
		Circuit c=new Circuit("Test");
		Rouleur r=new Rouleur("R");
		Sprinteur s=new Sprinteur("R");
		r.avancer(c.getCircuit().get(2));
		s.avancer(c.getCircuit().get(3));
		//Validation du resultat
		assertEquals("La cellule devrait etre normale de type 0", "N                 |    |    | \n"+"M                  \\    \\    \\ \n"+"D                 /    / RR / \n"+"A                 |    | SR | \n", c.toString());
	}
}
