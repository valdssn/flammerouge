
/**
 * Classe Carte
 * 
 */
public class Carte {

	/**
	 * attribut numero de la carte
	 */
	private int numero;

	/**
	 * Constructeur de la classe Carte a un entier
	 * 
	 * @param n
	 *            numero de la carte
	 */
	public Carte(int n) {
		this.numero = n;
	}

	/**
	 * Methode getNumero
	 * 
	 * @return le numero de la carte
	 */
	public int getNumero() {
		return this.numero;
	}

	/**
	 * methode toString
	 * 
	 * @return un string representant l'affichage de la carte
	 */

	public String toString() {
		return "vitesse : " + this.numero;
	}
}
