import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DeckRouleurTest {

	
	@Test
	/**
	 * Test constructeur du DeckRouleur qui verifie si toutes les cartes sont pr�sentes
	 */
	void test_Constructeur_DeckRouleur() {
		//Preparation des donnees
		DeckRouleur r=new DeckRouleur();
		//Validation du resultat
		assertEquals("la carte devrait porter le numero 3",3, r.getPioche().get(0).getNumero());
		assertEquals("la carte devrait porter le numero 3",3, r.getPioche().get(1).getNumero());
		assertEquals("la carte devrait porter le numero 3",3, r.getPioche().get(2).getNumero());
		assertEquals("la carte devrait porter le numero 4",4, r.getPioche().get(3).getNumero());
		assertEquals("la carte devrait porter le numero 4",4, r.getPioche().get(4).getNumero());
		assertEquals("la carte devrait porter le numero 4",4, r.getPioche().get(5).getNumero());
		assertEquals("la carte devrait porter le numero 5",5, r.getPioche().get(6).getNumero());
		assertEquals("la carte devrait porter le numero 5",5, r.getPioche().get(7).getNumero());
		assertEquals("la carte devrait porter le numero 5",5, r.getPioche().get(8).getNumero());
		assertEquals("la carte devrait porter le numero 6",6, r.getPioche().get(9).getNumero());
		assertEquals("la carte devrait porter le numero 6",6, r.getPioche().get(10).getNumero());
		assertEquals("la carte devrait porter le numero 6",6, r.getPioche().get(11).getNumero());
		assertEquals("la carte devrait porter le numero 7",7, r.getPioche().get(12).getNumero());
		assertEquals("la carte devrait porter le numero 7",7, r.getPioche().get(13).getNumero());
		assertEquals("la carte devrait porter le numero 7",7, r.getPioche().get(14).getNumero());
	}
	/**
	 * test la methode melanger qui melange une pioche
	 */
	@Test
	void test_melanger(){
		//Preparation des donnees
		DeckRouleur r=new DeckRouleur();
		DeckRouleur d=new DeckRouleur();
		//Methode testee
		d.melanger();
		boolean memecarte=true;
		for(int i=0;i<r.getPioche().size()-1;i++) {
			if(r.getPioche().get(i).getNumero()!=d.getPioche().get(i).getNumero()) {
				memecarte=false;
			}
		}
		//Validation du resultat
		assertEquals("Si memecarte est vrai alors le paquet n est pas melange",false,memecarte);
	}
	/**
	 * Test la methode ajouterCarteFatigue qui ajoute une carte vitesse 2 a une pioche
	 */
	@Test
	void Test_ajouterCarteFatigue() {
		//Preparation des donnees
		DeckRouleur r=new DeckRouleur();
		//Methode testee
		r.ajouterCarteFatigue();
		//Validation du resultat
		assertEquals("la pioche devrait contenir une carte vitesse 2",2,r.getPioche().get(15).getNumero());
	}
	/**
	 * test la methode piocher qui ajoute les 4 cartes a la main
	 */
	@Test
	void test_piocher() {
		//Preparation des donnees
		DeckRouleur r=new DeckRouleur();
		//Methode testee
		r.piocher();
		//la methode piocher prend les 4 cartes au dessus du paquet  
		//Validation du resultat
		assertEquals("la carte devrait porter le numero 7",7, r.getHand().get(0).getNumero());
		assertEquals("la carte devrait porter le numero 7",7, r.getHand().get(1).getNumero());
		assertEquals("la carte devrait porter le numero 7",7, r.getHand().get(2).getNumero());
		assertEquals("la carte devrait porter le numero 6",6, r.getHand().get(3).getNumero());
	}
	/**
	 * test la methode piocher quand il n y a plus assez de carte dans la pioche 
	 */
	@Test
	void test_piocher2() {
		//Preparation des donnees
		DeckRouleur r=new DeckRouleur();
		boolean cartediff=false;
		//Methode testee
		//15 cartes dnas la pioche
		r.piocher();
		//11 cartes dans la pioche
		r.piocher();
		//7 cartes dans la pioche
		r.piocher();
		//3 cartes dans la pioche
		r.piocher();
		//verifie si la derniere carte est differente de 3
		if(r.getHand().get(3).getNumero()!=3) {
			cartediff=true;
		}
		//reprend les 3 derni�res cartes de la pioche soit les 3 3 et une carte dans la defausse differente de 3 car les cartes 3 sont encore dans la pioche
		//Validation du resultat
		assertEquals("la carte devrait porter le numero 3",3, r.getHand().get(0).getNumero());
		assertEquals("la carte devrait porter le numero 3",3, r.getHand().get(1).getNumero());
		assertEquals("la carte devrait porter le numero 3",3, r.getHand().get(2).getNumero());
		assertEquals("Si cartediff est faux alors la carte n est pas differente de 3",true,cartediff);
	}
	/**
	 * test la methode defausser qui remet les cartes de la main dans la defausse
	 */
	@Test
	void test_defausser(){
		//Preparation des donnees
		DeckRouleur r=new DeckRouleur();
		r.piocher();
		//on test si les cartes sont dans la main 
		this.test_piocher();
		//Methode testee
		r.defausser();
		//Validation du resultat
		assertEquals("la carte devrait porter le numero 7",7, r.getDefausse().get(0).getNumero());
		assertEquals("la carte devrait porter le numero 7",7, r.getDefausse().get(1).getNumero());
		assertEquals("la carte devrait porter le numero 7",7, r.getDefausse().get(2).getNumero());
		assertEquals("la carte devrait porter le numero 6",6, r.getDefausse().get(3).getNumero());
	}
	
	
}
