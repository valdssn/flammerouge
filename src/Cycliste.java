
/**
 * Classe abstraite cycliste
 *
 */
public abstract class Cycliste {

	/**
	 * Attribut de type Cellule Position du cycliste
	 */
	private Cellule position;

	/*
	 * Attribut de type string Couleur du cycliste
	 */
	private String couleur;

	/**
	 * Constructeur de la classe Cycliste a un String
	 * 
	 * @param c
	 *            objet de type String
	 */
	public Cycliste(String c) {
		this.position = null;
		this.couleur = c;
	}

	/**
	 * Methode avancer qui fait avancer un cycliste sur une cellule
	 * 
	 * @param c
	 *            objet de type cellule
	 */
	public void avancer(Cellule c) {
		if (c.estLibreCellule()) {
			this.position = c;
			c.allerSurCellule(this);
		}
	}

	/**
	 * Methode getPosition
	 * 
	 * @return la position du cycliste
	 */
	public Cellule getPosition() {
		return this.position;
	}

	/**
	 * Methode setPosition qui attribue une position a un cycliste
	 * 
	 * @param c
	 *            objet de type cellule
	 */
	public void setPosition(Cellule c) {
		this.position = c;
	}

	/**
	 * Methode getCouleur
	 * 
	 * @return la couleur du cycliste
	 */
	public String getCouleur() {
		return this.couleur;
	}

	/**
	 * Methode abstraite afficher
	 * 
	 * @return un string qui affiche R si c'est un rouleur ou S si c'est un
	 *         sprinteur suivi de sa couleur
	 */
	public abstract String afficher();


}
