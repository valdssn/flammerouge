import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CyclisteTest {

	@Test
	/**
	 * Test de la methode avancer quand la cellule est vide
	 */
	void testAvancer1(){
		//Preparation des donnees
		Sprinteur s = new Sprinteur ("B");
		Cellule c = new Cellule(0);
		//Methode testee
		s.avancer(c);
		//Validation du resultat
		assertEquals("La position du cycliste ne devrait pas etre celle ci", c, s.getPosition());
		assertEquals("La  case droite de la cellule devrait contenir le cycliste", s, c.getCaseDroite().getCycliste());
	}
	
	@Test
	/**
	 * Test de la methode avancer quand on fait avancer un premier cycliste puis un autre 
	 */
	void testAvancer2(){
		//Preparation des donnees
		Sprinteur s = new Sprinteur ("B");
		Rouleur r = new Rouleur("V");
		Cellule c = new Cellule(0);		
		//Methode testee 
		s.avancer(c);
		r.avancer(c);
		//Vaidation du resultat
		assertEquals("La position du sprinteur ne devrait pas etre celle ci", c, s.getPosition());
		assertEquals("La position du rouleur ne devrait pas etre celle ci", c, r.getPosition());
		assertEquals("La  case droite de la cellule devrait contenir le sprinteur", s, c.getCaseDroite().getCycliste());
		assertEquals("La  case gauche de la cellule devrait contenir le rouleur", r, c.getCaseGauche().getCycliste());
	}
	
	@Test
	/**
	 * Test de la methode avancer quand on fait avancer deux cyclistes puis apres un troisieme
	 */
	void testAvancer3(){
		//Preparation des donnees
		Sprinteur s1 = new Sprinteur ("B");
		Rouleur r = new Rouleur("V");
		Sprinteur s2 = new Sprinteur ("J");
		Cellule c = new Cellule(0);	
		//Methode testee
		s1.avancer(c);
		r.avancer(c);
		s2.avancer(c);
		//Validation du resultat
		assertEquals("La position du sprinteur 1 ne devrait pas etre celle ci", c, s1.getPosition());
		assertEquals("La position du rouleur ne devrait pas etre celle ci", c, r.getPosition());
		assertEquals("La  case droite de la cellule devrait contenir le sprinteur", s1, c.getCaseDroite().getCycliste());
		assertEquals("La  case gauche de la cellule devrait contenir le rouleur", r, c.getCaseGauche().getCycliste());
		assertEquals("Le sprinteur 2 ne devrait pas avoir avance", null, s2.getPosition());
	}
	
	@Test
	/**
	 * Test de la methode afficherRouleur
	 */
	void testAfficherRouleur() {
		//Preparation des donnees
		Rouleur r = new Rouleur("V");
		//Validation du resultat
		assertEquals("L'affichage ne devrait pas etre celui ci", "RV", r.afficher());
	}
	
	@Test
	/**
	 * Test de la methode afficherSprinteur
	 */
	void testAfficherSprinteur() {
		//Preparation des donnees
		Sprinteur s = new Sprinteur("J");
		//Validation du resultat
		assertEquals("L'affichage ne devrait pas etre celui ci", "SJ", s.afficher());
	}
	

}
