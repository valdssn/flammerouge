import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Classe Circuit
 *
 */
public class Circuit {

	/**
	 * attribut de type bufferedReader
	 * 
	 */
	BufferedReader br;

	/**
	 * attribut de type ArrayListe. Liste d'objets de type Cellule
	 */
	ArrayList<Cellule> cell = new ArrayList<Cellule>();

	/**
	 * Constructeur de la classe circuit a un String
	 * 
	 * @param nom
	 *            nom du circuit
	 */
	public Circuit(String nom) {
		try {
			this.br = new BufferedReader(new FileReader("./donnees/" + nom + ".txt"));
			this.creerCircuit();
		} catch (FileNotFoundException e) {
			System.out.println("Fichier inexistant");
		}
	}

	/**
	 * Methode creerCircuit qui cree un circuit
	 */
	private void creerCircuit() {
		String s;
		try {
			s = br.readLine();
			int longueur = s.length();
			for (int i = 0; i < longueur; i++) {
				char c = s.charAt(i);
				cell.add(new Cellule(Character.getNumericValue(c)));
			}
		} catch (IOException e) {
			System.out.println("erreur lors de la lecture du fichier");
		}

	}

	/**
	 * Methode getCircuit
	 * 
	 * @return une liste d'objets de type Cellule
	 */
	public ArrayList<Cellule> getCircuit() {
		return this.cell;
	}

	/*
	 * Methode toString qui affiche le circuit avec les joueurs
	 */
	public String toString() {
		String espace = "                 ";
		String s = "";
		String type;
		String couleur;
		for (int i = 0; i < cell.size(); i++) {
			if (this.cell.get(i).getTypeCase() == 0) {
				if (this.cell.get(i).getCaseGauche().getCycliste() == null
						&& this.cell.get(i).getCaseDroite().getCycliste() != null) {

					s = s + "N" + espace + "|    | " + this.cell.get(i).getCaseDroite().getCycliste().afficher() + " | "
							+ "\n";

				} else if (this.cell.get(i).getCaseGauche().getCycliste() != null
						&& this.cell.get(i).getCaseDroite().getCycliste() != null) {

					s = s + "N" + espace + "| " + this.cell.get(i).getCaseGauche().getCycliste().afficher() + " | "
							+ this.cell.get(i).getCaseDroite().getCycliste().afficher() + " | " + "\n";
				} else {
					s = s + "N" + espace + "|    |" + "    | " + "\n";
				}

			} else if (this.cell.get(i).getTypeCase() == 1) {

				espace = espace + " ";

				if (this.cell.get(i).getCaseGauche().getCycliste() == null
						&& this.cell.get(i).getCaseDroite().getCycliste() != null) {

					s = s + "M" + espace + "\\    \\ " + this.cell.get(i).getCaseDroite().getCycliste().afficher()
							+ " \\ " + "\n";

				} else if (this.cell.get(i).getCaseGauche().getCycliste() != null
						&& this.cell.get(i).getCaseDroite().getCycliste() != null) {

					s = s + "M" + espace + "\\ " + this.cell.get(i).getCaseGauche().getCycliste().afficher() + " \\ "
							+ this.cell.get(i).getCaseDroite().getCycliste().afficher() + " \\ " + "\n";
				} else {
					s = s + "M" + espace + "\\    \\" + "    \\ " + "\n";
				}

			} else if (this.cell.get(i).getTypeCase() == 2) {

				int longueur = espace.length();
				espace = "";
				for (int j = 0; j < longueur - 1; j++) {
					espace = espace + " ";

				}
				if (this.cell.get(i).getCaseGauche().getCycliste() == null
						&& this.cell.get(i).getCaseDroite().getCycliste() != null) {

					s = s + "D" + espace + "/    / " + this.cell.get(i).getCaseDroite().getCycliste().afficher() + " / "
							+ "\n";

				} else if (this.cell.get(i).getCaseGauche().getCycliste() != null
						&& this.cell.get(i).getCaseDroite().getCycliste() != null) {

					s = s + "D" + espace + "/ " + this.cell.get(i).getCaseGauche().getCycliste().afficher() + " / "
							+ this.cell.get(i).getCaseDroite().getCycliste().afficher() + " / " + "\n";
				} else {
					s = s + "D" + espace + "/    /" + "    / " + "\n";
				}
			} else if (this.cell.get(i).getTypeCase() == 3) {

				if (this.cell.get(i).getCaseGauche().getCycliste() == null
						&& this.cell.get(i).getCaseDroite().getCycliste() != null) {

					s = s + "A" + espace + "|    | " + this.cell.get(i).getCaseDroite().getCycliste().afficher() + " | "
							+ "\n";

				} else if (this.cell.get(i).getCaseGauche().getCycliste() != null
						&& this.cell.get(i).getCaseDroite().getCycliste() != null) {

					s = s + "A" + espace + "| " + this.cell.get(i).getCaseGauche().getCycliste().afficher() + " | "
							+ this.cell.get(i).getCaseDroite().getCycliste().afficher() + " | " + "\n";
				} else {
					s = s + "A" + espace + "|    |" + "    | " + "\n";
				}
			}
		}
		return s;
	}

}
