
/**
 * Class Joueur
 *
 */

public class Joueur {

	/**
	 * Attributs de type String nom du joueur et couleur du joueur
	 */
	private String nomJoueur, couleur;

	/**
	 * Attribut de type Rouleur cycliste rouleur du joueur
	 */
	private Rouleur rouleur;

	/**
	 * Attribut de type Sprinteur cycliste sprinteur du joueur
	 */
	private Sprinteur sprinteur;

	/**
	 * Attribut de type DeckRouleur paquet de cartes du cycliste rouleur
	 */
	private DeckRouleur deckRouleur;

	/**
	 * Attribut de type DeckSprinteur paquet de cartes du cycliste Sprinteur
	 */
	private DeckSprinter deckSprinteur;

	/**
	 * Constructeur de la classe Joueur
	 * 
	 * @param n
	 *            nom du joueur
	 * @param r
	 *            cycliste rouleur du joueur
	 * @param s
	 *            cycliste sprinteur du joueur
	 * @param dr
	 *            paquet de cartes du rouleur
	 * @param ds
	 *            paquet de cartes du sprinteur
	 * @param c
	 *            couleur du joueur
	 */
	public Joueur(String n, Rouleur r, Sprinteur s, DeckRouleur dr, DeckSprinter ds, String c) {
		this.nomJoueur = n;
		this.rouleur = r;
		this.sprinteur = s;
		this.deckRouleur = dr;
		this.deckSprinteur = ds;
		this.couleur = c;
	}

	/**
	 * Methode getRouleur
	 * 
	 * @return le cycliste rouleur du joueur
	 */
	public Rouleur getRouleur() {
		return this.rouleur;
	}

	/**
	 * Methode getSprinteur
	 * 
	 * @return le cycliste sprinteur du joueur
	 */
	public Sprinteur getSprinteur() {
		return this.sprinteur;
	}

	/**
	 * Methode getPaquetRouleur
	 * 
	 * @return le paquet de cartes du rouleur
	 */
	public DeckRouleur getPaquetRouleur() {
		return this.deckRouleur;
	}

	/**
	 * Methode getPaquetSprinteur
	 * 
	 * @return le paquet de cartes du sprinteur
	 */
	public DeckSprinter getPaquetSprinteur() {
		return this.deckSprinteur;
	}

	/**
	 * Methode getNom
	 * 
	 * @return le nom du joueur
	 */
	public String getNom() {
		return this.nomJoueur;
	}

	/**
	 * Methode getCouleur
	 * 
	 * @return la couleur du joueur en entier
	 */
	public String getCouleur() {
		String s = "";
		if (this.couleur == "R") {
			s = "rouge";
		} else if (this.couleur == "B") {
			s = "bleu";
		} else if (this.couleur == "V") {
			s = "vert";
		} else if (this.couleur == "J") {
			s = "jaune";
		}
		return s;
	}

	/**
	 * Methode afficher couleur
	 * 
	 * @return la premiere lettre de la rouleur du joueur
	 */
	public String afficherCouleur() {
		return this.couleur;
	}
}
