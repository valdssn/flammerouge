import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarteEnergieTest {
	
	@Test
	/**
	 * test du constructeur CarteEnergie
	 */
	void test_constructeur_CarteEnergie() {
		//Preparation des donnees 
		CarteEnergie carte=new CarteEnergie(2);
		//Validation du resultat
		assertEquals("la carte devrait etre egal a 2", 2, carte.getNumero());
	}

}
