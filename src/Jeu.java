import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * classe Jeu
 *
 */
public class Jeu {

	/**
	 * Attribut de type ArrayList. Liste d'objets de type Joueur
	 */
	private ArrayList<Joueur> listeJoueur;

	/**
	 * Attribut de type booleen
	 */
	private boolean partieTerminee;

	/**
	 * Attribut de type scanner
	 */
	Scanner sc = new Scanner(System.in);

	/**
	 * Attribut tableau d'objets de type String
	 */
	private String[] couleurs = { "R", "B", "V", "J" };

	/**
	 * Attribut de type Circuit
	 */
	private Circuit circ;

	/**
	 * Attribut de type Int
	 */
	int nbJoueurs;

	/**
	 * Constructeur de la classe Jeu
	 */

	public Jeu() {

		// On entre le nombre de joueur de la partie
		System.out.println("Combien de joueur ?");
		//On test si la saisie est bien un entier si ce n'est pas le cas on redemande
		boolean saisieCorrectNbJoueur = false;
		while (!saisieCorrectNbJoueur) {
			try {
				nbJoueurs = sc.nextInt();
				saisieCorrectNbJoueur = true;

			} catch (InputMismatchException IME) {
				System.out.println("Un entier est attendu");
				saisieCorrectNbJoueur = false;
				sc.nextLine();
			}
		}
		sc.nextLine();

		// On cree une liste de joueurs de la taille du nombre de joueurs
		this.listeJoueur = new ArrayList<Joueur>(nbJoueurs);

		// Pour chaque joueur on demande son nom
		for (int i = 0; i < nbJoueurs; i++) {
			System.out.println("Nom du joueur ");

			String nomJoueur = sc.nextLine();

			// On ajoute le joueur dans la liste
			this.listeJoueur.add(new Joueur(nomJoueur, new Rouleur(couleurs[i]), new Sprinteur(couleurs[i]),
					new DeckRouleur(), new DeckSprinter(), couleurs[i]));

		}

		// On demande si les joueurs veulent creer leur propre circuit ou pas
		System.out.println("Souhaitez vous creer votre circuit ?");
		String s = "";
		while (!s.equals("oui") && !s.equals("non")) {
			System.out.println("Entrez oui ou non");
			s = sc.nextLine();
		}

		// Si oui
		if (s.equals("oui")) {
			String nom;

			// On demande le nom du circuit
			System.out.println("Entrez le nom du circuit");
			nom = sc.nextLine();
			String circuit;

			// On demande de rentrer les differentes cellules avec 0 pour une route plate, 1
			// pour une montee, 2 pour une decente et 3 pour l'arrivee
			System.out.println(
					"Creez votre circuit avec 0 pour route plate, 1 pour montee, 2 pour descente et n oubliez pas 3 pour l arrivee");
			circuit = sc.nextLine();

			// On cree un fichier BufferWriter dans lequel on ecrit le String contenant les
			// cellules
			BufferedWriter b;
			try {
				b = new BufferedWriter(new FileWriter("./donnees/" + nom + ".txt"));
				b.write(circuit);
				b.close();
			} catch (IOException e) {
				System.out.println("Erreur lors de la lecture du fichier");
			}

			this.circ = new Circuit(nom);

			// Sinon si on ne veut pas creer de circuit 
		} else {

			// On affiche le nom de tous les circuits disponibles
			File f = new File("./donnees");
			for (String nom : f.list()) {

				System.out.println(nom);

			}
			String choix;

			// On demande sur quel circuit les joueurs veulent jouer
			System.out.println("Quel circuit choisissez vous ? (entrez le nom du circuit sans le .txt)");
			choix = sc.nextLine();
			this.circ = new Circuit(choix);
		}

		// On parcours tout le circuit et on place les joueurs sur les premieres
		// cellules. Joueur 1 sur cellule 1, joueur 2 sur cellule 2 etc...
		for (int i = 0; i < this.listeJoueur.size(); i++) {
			this.circ.getCircuit().get(i).allerSurCellule(this.listeJoueur.get(i).getSprinteur());
			this.circ.getCircuit().get(i).allerSurCellule(this.listeJoueur.get(i).getRouleur());
			this.listeJoueur.get(i).getSprinteur().setPosition(this.circ.getCircuit().get(i));
			this.listeJoueur.get(i).getRouleur().setPosition(this.circ.getCircuit().get(i));
		}

		this.partieTerminee = false;
		//On lance le deroulement du jeu 
		this.deroulementJeu();

	}

	/**
	 * Methode phaseAspiration Si un cycliste a une cellule vide entre lui et le
	 * cycliste de devant alors il avance d'une cellule Pas d'aspiration dans les
	 * montees
	 */
	public void phaseAspiration() {
		for (int i = this.circ.getCircuit().size() - 4; i >= 0; i--) {
			if (this.circ.getCircuit().get(i).getTypeCase() != 1) {
				Cycliste cyclistederriere;
				if (this.circ.getCircuit().get(i).celluleOccupeDroite()) {

					if (this.circ.getCircuit().get(i + 2).celluleOccupeDroite()) {
						if (!this.circ.getCircuit().get(i + 1).celluleOccupeDroite()) {
							cyclistederriere = this.circ.getCircuit().get(i).getCyclisteCelluleDroite();
							cyclistederriere.getPosition().viderCellule(cyclistederriere);
							cyclistederriere.avancer(this.circ.getCircuit().get(i + 1));

							if (this.circ.getCircuit().get(i).celluleOccupeGauche()) {
								Cycliste cyclistegauche = this.circ.getCircuit().get(i).getCyclisteCelluleGauche();
								cyclistegauche.getPosition().viderCellule(cyclistegauche);
								cyclistegauche.avancer(this.circ.getCircuit().get(i));
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Methode trierJoueur qui trie les joueurs en fonction de la position de leurs
	 * cyclistes
	 * 
	 * @return une Arraylist d'objet de type Joueur
	 */
	public ArrayList<Joueur> trierJoueur() {
		String couleurCourant;
		ArrayList<Joueur> listetmp = new ArrayList<Joueur>();
		for (int i = this.circ.getCircuit().size() - 1; i >= 0; i--) {
			if (this.circ.getCircuit().get(i).celluleOccupeDroite()) {
				couleurCourant = this.circ.getCircuit().get(i).getCyclisteCelluleDroite().getCouleur();
				for (int j = 0; j < this.listeJoueur.size(); j++) {
					if (couleurCourant.equals(this.listeJoueur.get(j).afficherCouleur())) {
						if (listetmp != null) {
							if (!listetmp.contains(this.listeJoueur.get(j))) {
								listetmp.add(this.listeJoueur.get(j));
							}
						}

					}
				}
				if (this.circ.getCircuit().get(i).celluleOccupeGauche()) {
					couleurCourant = this.circ.getCircuit().get(i).getCyclisteCelluleGauche().getCouleur();
					for (int j = 0; j < this.listeJoueur.size(); j++) {
						if (couleurCourant.equals(this.listeJoueur.get(j).afficherCouleur())) {
							if (listetmp != null) {
								if (!listetmp.contains(this.listeJoueur.get(j))) {
									listetmp.add(this.listeJoueur.get(j));
								}
							}

						}
					}
				}
			}
		}
		return listetmp;
	}

	/**
	 * Methode demarrerJeu mise en place de la partie, les joueurs piochent et
	 * melangent leurs paquets de cartes
	 */
	public void demarrerJeu() {

		for (int i = 0; i < this.listeJoueur.size(); i++) {
			Joueur joueurCourant = this.listeJoueur.get(i);
			System.out.println("Joueur " + this.listeJoueur.get(i).getNom() + " de couleur "
					+ this.listeJoueur.get(i).getCouleur() + " doit jouer");
			System.out.println("Cartes du rouleur: ");
			joueurCourant.getPaquetRouleur().melanger();
			joueurCourant.getPaquetRouleur().piocher();
			joueurCourant.getPaquetRouleur().choisirCarte();
			System.out.println("Cartes du sprinteur: ");
			joueurCourant.getPaquetSprinteur().melanger();
			joueurCourant.getPaquetSprinteur().piocher();
			joueurCourant.getPaquetSprinteur().choisirCarte();
		}
	}

	/**
	 * Methode phaseEnergie recupere le sprinteur et l'energie du sprinteur et le
	 * rouleur et l'energie du rouleur du joueur courant et applique la methode
	 * energie
	 */
	public void phaseEnergie() {
		for (int i = 0; i < this.listeJoueur.size(); i++) {
			int choixCycliste;
			Sprinteur sprinteur = this.listeJoueur.get(i).getSprinteur();
			int energieSprinteur = this.listeJoueur.get(i).getPaquetSprinteur().getHand().get(0).getNumero();

			Rouleur rouleur = this.listeJoueur.get(i).getRouleur();
			int energieRouleur = this.listeJoueur.get(i).getPaquetRouleur().getHand().get(0).getNumero();

			System.out.println("Joueur " + this.listeJoueur.get(i).getNom()
					+ ", quel cycliste souhaitez vous jouer en premier ? 1. Sprinteur, 2.Rouleur");
			
			//On test si la saisie est bien un entier si ce n'est pas le cas on recommence 
			boolean saisieCycliste = false;
			while (!saisieCycliste) {
				try {
					choixCycliste = sc.nextInt();
					saisieCycliste = true;
					if (choixCycliste == 1) {
						this.energie(sprinteur, energieSprinteur);
						this.energie(rouleur, energieRouleur);
					} else if (choixCycliste == 2) {
						this.energie(rouleur, energieRouleur);
						this.energie(sprinteur, energieSprinteur);
					}
				} catch (InputMismatchException IME) {
					System.out.println("Un entier est attendu");
					saisieCycliste = false;
					sc.nextLine();

				}
			}
		

		}
	}

	/**
	 * Methode energie qui fait avancer un cycliste en fonction de son energie et du
	 * circuit
	 * 
	 * @param c
	 *            objet de type Cycliste
	 * @param energie
	 *            objet de type entier
	 */
	public void energie(Cycliste c, int energie) {

		// On recupere le maximum et le minimum de nombre de cellule que le cycliste
		// peut avancer en fonction du terrain
		int max = c.getPosition().getMax();
		int min = c.getPosition().getMin();

		// si l'energie est superieure au maximum alors on mets l'energie au maximum
		if (energie > max) {
			energie = max;
		}

		// De meme pour le minimum
		if (energie < min) {
			energie = min;
		}

		int j = 0;
		// Tant que l'iterateur j est inferieur au nombre de deplacement possible alors
		// on continue
		while (j < energie) {

			// On recupere la position du cycliste et l'index de sa position
			int ind = this.circ.getCircuit().indexOf(c.getPosition());
			Cellule position = c.getPosition();

			// On verifie que la cellule sur laquelle le cycliste se trouve n'est pas
			// l'arrivee
			if (position.getTypeCase() != 3) {

				// On recupere la prochaine cellule
				Cellule prochaineCellule = this.circ.getCircuit().get(ind + 1);
				// Si elle est libre on enleve le cycliste de la cellule courante, on deplace le
				// cycliste de
				// gauche s'il y en a un et on fait avancer le cycliste sur la case suivante
				if (prochaineCellule.estLibreCellule()) {
					c.getPosition().viderCellule(c);
					c.getPosition().decallerJoueur();
					c.avancer(prochaineCellule);
					// on incremente j
					j++;

					// Si la prochaine cellule n'est pas libre
				} else {

					// on verifie que j sois toujours inferieur au nombre de deplacement possible
					if (j < energie) {

						// On calcule le nombre de deplacement restant
						int deplacementRestant = energie - j;

						// on cree un nouvel iterateur qui represente les deplacements
						int ca = 1;
						boolean trouvePlace = false;

						// Tant que l'iterateur ca est inferieur au nombre de deplacement restant et que
						// trouvePlace est faux
						while (ca <= deplacementRestant && !trouvePlace) {

							// on verifie que la cellule suivante n'est pas la case arrivee
							if (this.circ.getCircuit().get(ind + ca).getTypeCase() != 3) {

								// on verifie si la cellule est libre
								if (this.circ.getCircuit().get(ind + ca).estLibreCellule()) {
									// trouve place vaut vrai
									trouvePlace = true;

									// sinion on incremente ca
								} else {
									ca++;
								}

								// sinon partieTerminee vaut vrai et on arrete la boucle
							} else {
								partieTerminee = true;
								break;
							}
						}

						// si on a pas trouve de place on arrete la boucle
						if (!trouvePlace) {
							break;

							// Si on a trouve une place on fait avancer le cycliste
						} else {
							c.getPosition().viderCellule(c);
							c.getPosition().decallerJoueur();
							c.avancer(this.circ.getCircuit().get(ind + ca));

							// on ajoute a j le nombre de deplacement ca effectue
							j = j + ca;
						}

					}

				}
				// Si le cycliste se trouve sur l'arrivee alors la partie est terminee
			} else {
				this.partieTerminee = true;
				break;
			}

			// si j depasse le nombre de deplacements autorises alors on arrete la boucle
			if (j >= c.getPosition().getMax()) {
				break;
			}
		}
	}

	/**
	 * Methode phasefatigue qui recupere le joueur courant et qui applique la
	 * methode fatigue sur son sprinteur et son rouleur
	 */
	public void phaseFatigue() {
		for (int i = 0; i < this.listeJoueur.size(); i++) {
			Joueur joueurCourant = this.listeJoueur.get(i);
			this.fatigue(joueurCourant.getSprinteur(), joueurCourant);
			this.fatigue(joueurCourant.getRouleur(), joueurCourant);
		}
	}

	/**
	 * Methode fatigue Si un cycliste a deux cellules vide devant lui alors il
	 * re�ois une carte fatigue dans son paquet
	 * 
	 * @param c
	 *            objet de type Cycliste
	 * @param j
	 *            objet de type Joueur
	 */
	public void fatigue(Cycliste c, Joueur j) {
		// On recupere la position du cycliste
		Cellule celluleCourante = c.getPosition();
		// On recupere l'index de la position
		int index = this.circ.getCircuit().indexOf(celluleCourante);
		// On verifie que le joueur ne sois pas sur la case d'arrivee
		if (this.circ.getCircuit().get(index).getTypeCase() != 3) {
			// On verifie que la case suivante n'est pas la case d'arrivee
			if (this.circ.getCircuit().get(index + 1).getTypeCase() != 3) {
				// On verifie si la cellule suivante est libre a droite et a gauche
				if (!this.circ.getCircuit().get(index + 1).celluleOccupeDroite()
						&& !this.circ.getCircuit().get(index + 1).celluleOccupeGauche()) {
					// Si le cycliste est un rouleur
					if (c instanceof Rouleur) {
						j.getPaquetRouleur().ajouterCarteFatigue();
						// Si le cycliste est un sprinteur
					} else {
						j.getPaquetSprinteur().ajouterCarteFatigue();
					}

				}
			}
		}
	}

	/**
	 * Methode sauvegarderResultat qui sauvegarde dans un fichier les resultats de
	 * la partie
	 * 
	 * @param s
	 *            objet de type String
	 * @param j
	 *            objet de type ArrayList de Joueur
	 */
	public void sauvegarderResultat(String s, ArrayList<Joueur> j) {
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter("Resultat.txt"));
			bw.write(s);
			bw.newLine();
			int i = 0;
			for (Joueur joueur : j) {
				bw.write("Le joueur en position " + (i + 1) + " est: " + joueur.getNom());
				bw.newLine();
				i++;
			}
			bw.write("Le gagnant est " + this.listeJoueur.get(0).getNom());
			bw.newLine();
			bw.write("Felicitations ! :) ");

			bw.close();
		} catch (IOException e) {
			System.out.println("Erreur lors de la lecture du fichier");
		}

	}

	/**
	 * Methode estFiniePartie
	 * 
	 * @return true si la partie est terminee
	 */
	public boolean estFiniePartie() {
		boolean fini = false;
		if ((this.circ.getCircuit().get(this.circ.getCircuit().size() - 1).celluleOccupeGauche())
				|| (this.circ.getCircuit().get(this.circ.getCircuit().size() - 1).celluleOccupeDroite())) {
			fini = true;
		}
		return fini;
	}

	/**
	 * Methode deroulementJeu qui applique les differentes phases de la partie et le
	 * classement des joueurs
	 */
	public void deroulementJeu() {
		System.out.println(this.circ.toString());
		for (int i = 0; i < this.listeJoueur.size(); i++) {
			System.out.println("Le joueur " + (i + 1) + "  " + this.listeJoueur.get(i).getNom() + " est de couleur "
					+ this.listeJoueur.get(i).getCouleur());
		}
		while (!partieTerminee) {

			this.demarrerJeu();
			this.phaseEnergie();
			this.phaseAspiration();
			this.phaseFatigue();
			System.out.println(this.circ.toString());
			if (this.estFiniePartie()) {
				this.partieTerminee = true;
			}
			this.listeJoueur = trierJoueur();
			for (int i = 0; i < this.listeJoueur.size(); i++) {
				System.out.println("Le joueur " + this.listeJoueur.get(i).getNom() + " de couleur "
						+ this.listeJoueur.get(i).getCouleur() + " est en position " + (i + 1));

			}

		}
		System.out.println("Partie terminee !");
		this.sauvegarderResultat(this.circ.toString(), this.listeJoueur);
	}


}
