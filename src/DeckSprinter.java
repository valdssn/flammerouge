import java.util.*;

public class DeckSprinter {
	/**
	 * Attribut de type ArrayList. Liste d'objets de type carte pour la pioche
	 */
	private ArrayList<Carte> pioche = new ArrayList<Carte>();
	/**
	 * Attribut de type ArrayList. Liste d'objets de type carte pour la defausse
	 */
	private ArrayList<Carte> defausse = new ArrayList<Carte>();
	/**
	 * Attribut de type ArrayList. Liste d'objets de type carte pour la main
	 */
	private ArrayList<Carte> hand = new ArrayList<Carte>();

	/**
	 * Constructeur de la classe DeckSprinter sans parametre qui cree un paquet de
	 * carte
	 */
	public DeckSprinter() {
		int vitesse = 1;
		for (int i = 0; i <= 4; i++) {
			vitesse = vitesse + 1;
			if (vitesse == 6) {
				vitesse = 9;
			}
			for (int j = 0; j < 3; j++) {
				CarteEnergie carte = new CarteEnergie(vitesse);
				this.pioche.add(carte);
			}
		}
	}

	/**
	 * methode melanger qui melange le deck
	 */
	public void melanger() {
		Collections.shuffle(this.pioche);
	}

	/**
	 * methode ajouterCarteFatigue qui ajoute une cartefatigue au deck
	 */
	public void ajouterCarteFatigue() {
		CarteFatigue cartef = new CarteFatigue();
		this.pioche.add(cartef);
	}

	/**
	 * methode piocher qui pioche 4 cartes dans un deck pour les ajouter a la main
	 */
	public void piocher() {
		// remet la carte choisis dans la defausse
		for (int i = 0; i < this.hand.size(); i++) {
			this.defausse.add(this.hand.get(i));
		}
		this.hand.clear();

		if (this.pioche.size() <= 4) {
			int nombrecarte = this.pioche.size();
			for (int j = 0; j < nombrecarte; j++) {
				hand.add(this.pioche.get(this.pioche.size() - 1));
				this.pioche.remove(this.pioche.size() - 1);
			}
			int besoincarte = 3 - nombrecarte;
			this.pioche.addAll(this.defausse);
			this.defausse.clear();
			melanger();

			for (int i = 0; i <= besoincarte; i++) {
				hand.add(this.pioche.get(this.pioche.size() - 1));
				this.pioche.remove(this.pioche.size() - 1);
			}

		} else {
			for (int i = 0; i < 4; i++) {
				hand.add(this.pioche.get(this.pioche.size() - 1));
				this.pioche.remove(this.pioche.size() - 1);
			}
		}
	}

	/**
	 * methode choisirCarte qui permet de choisir entre les 4 cartes dans la main
	 */
	public void choisirCarte() {
		int num = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Choisir une carte parmis les cartes suivante :");
		// defile les carte de la main
		for (int i = 0; i < 4; i++) {
			num = num + 1;
			System.out.println(num + ":" + this.hand.get(i));
		}

		System.out.println("Entrez le numero de la carte :");
		int numcarte = 0;

		while (numcarte != 1 && numcarte != 2 && numcarte != 3 && numcarte != 4) {
			System.out.println("Entrez 1, 2, 3 ou 4");
			boolean saisieCorrectCarte = false;

			while (!saisieCorrectCarte) {
				try {
					numcarte = sc.nextInt();
					saisieCorrectCarte = true;

				} catch (InputMismatchException IME) {
					System.out.println("Un entier est attendu");
					saisieCorrectCarte = false;
					sc.nextLine();
				}
			}
			sc.nextLine();
		}
		numcarte = numcarte - 1;
		Carte cartechoisie = this.hand.get(numcarte);

		for (int j = 0; j < hand.size(); j++) {
			Carte cartecourante = this.hand.get(j);
			if (j != numcarte) {
				this.defausse.add(cartecourante);
			}
		}
		this.hand.clear();
		this.hand.add(cartechoisie);
	}

	/**
	 * methode defausser qui remet les cartes pioche dans la defausse
	 */

	public void defausser() {
		for (int i = 0; i < this.hand.size(); i++) {
			this.defausse.add(this.hand.get(i));
		}
		this.hand.clear();
	}

	/**
	 * Methode toStringHand
	 * 
	 * @return un string contenant les carte(s) dans la main
	 */
	public String toStringHand() {
		String s = "Carte(s) dans la main : " + hand.get(0).toString();
		for (int i = 1; i < this.hand.size(); i++) {
			s = s + " " + hand.get(i).toString();
		}
		return s;
	}

	/**
	 * Methode toStringDefausse
	 * 
	 * @return un string contenant les carte(s) dans la defausse
	 */
	public String toStringDefausse() {
		String s = "Carte(s) dans la defausse : " + defausse.get(0).toString();
		for (int i = 1; i < this.defausse.size(); i++) {
			s = s + " " + defausse.get(i).toString();
		}
		return s;
	}

	/**
	 * Methode toString *
	 * 
	 * @returnun string contenant les carte(s) dans la pioche
	 */
	public String toString() {
		String s = "Carte(s) dans la pioche : " + pioche.get(0).toString();
		for (int i = 1; i < this.pioche.size(); i++) {
			s = s + " " + pioche.get(i).toString();
		}
		return s;
	}

	/**
	 * Methode getPioche
	 * 
	 * @return la liste de la pioche
	 */
	public ArrayList<Carte> getPioche() {
		return this.pioche;
	}

	/**
	 * Methode getHand
	 * 
	 * @return la liste de la main
	 */
	public ArrayList<Carte> getHand() {
		return this.hand;
	}

	/**
	 * Methode getDefausse
	 * 
	 * @return la liste de la defausse
	 */
	public ArrayList<Carte> getDefausse() {
		return this.defausse;
	}

}
