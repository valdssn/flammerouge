/**
 * 
 * Classe Rouleur qui extends la classe Cycliste
 *
 */
public class Rouleur extends Cycliste {

	/**
	 * Constructeur de la classe Rouleur a un String
	 * 
	 * @param c
	 *            couleur du rouleur
	 */
	public Rouleur(String c) {
		super(c);
	}

	/**
	 * Methode afficher
	 * 
	 * @return un String affichant R plus sa couleur
	 */
	public String afficher() {
		return "R" + this.getCouleur();
	}

}
