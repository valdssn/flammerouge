import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class JoueurTest {
	
	@Test
	/**
	 * test du constructeur Joueur
	 */
	void test_constructeur_Joueur() {
		Rouleur r=new Rouleur("R");
		Sprinteur s=new Sprinteur("R");
		DeckRouleur dr=new DeckRouleur();
		DeckSprinter ds=new DeckSprinter();
		//Methode testee
		Joueur j=new Joueur("Johnny",r,s,dr,ds,"R");
		//Validation du resultat
		assertEquals("le nom du joueur devrais etre johnny", "Johnny", j.getNom());
		assertEquals("le rouleur devrait etre le rouleur r", r, j.getRouleur());
		assertEquals("le sprinteur devrait etre le sprinteur s", s, j.getSprinteur());
		assertEquals("le deckrouleur devrait etre le rouleur dr", dr, j.getPaquetRouleur());
		assertEquals("le decksprinteur devrait etre le sprinteur ds", ds, j.getPaquetSprinteur());
		assertEquals("le couleur devrait etre R", "R", j.afficherCouleur());
	}
	
	@Test
	/**
	 * test de la methode getCouleur
	 */
	void test_getCouleur() {
		//Preparation des donnees
		Rouleur r=new Rouleur("R");
		Sprinteur s=new Sprinteur("R");
		DeckRouleur dr=new DeckRouleur();
		DeckSprinter ds=new DeckSprinter();
		Joueur j=new Joueur("Johnny",r,s,dr,ds,"R");
		//Validation du resultat
		assertEquals("le couleur devrait etre rouge", "rouge", j.getCouleur());
	}

}
