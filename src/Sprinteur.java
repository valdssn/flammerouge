/**
 * 
 * Classe Sprinteur qui extends la classe Cycliste
 *
 */
public class Sprinteur extends Cycliste {

	/**
	 * Constructeur de la classe Sprinteur a un String
	 * 
	 * @param c
	 *            couleur du sprinteur
	 */
	public Sprinteur(String c) {
		super(c);
	}

	/**
	 * Methode afficher
	 * 
	 * @return un String affichant S plus sa couleur
	 */
	public String afficher() {
		return "S" + this.getCouleur();
	}
}
