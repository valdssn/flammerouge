
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CelluleTest {

	@Test
	/**
	 * Test de la methode allerSurCellule quand la cellule est vide 
	 */
	void testAlleSurCellule1() {
		//Preparation des donnees
		Cellule c = new Cellule(0);
		Sprinteur s = new Sprinteur ("R");
		//Methode testee
		c.allerSurCellule(s);
		//Validation du resultat
		assertEquals("Il devrait y avoir le sprinteur rouge sur la case droite de la cellule", s, c.getCaseDroite().getCycliste());
	}
	
	@Test
	/**
	 * Test de la methode allerSurCellule quand la cellule est occupee quand on veut ajouter deux joueurs dans la cellule
	 */
	void testAlleSurCellule2() {
		//Preparation des donnees
		Cellule c = new Cellule(0);
		Sprinteur s = new Sprinteur ("R");
		Rouleur r = new Rouleur("B");
		//Methode testee
		c.allerSurCellule(s);
		c.allerSurCellule(r);
		//Validation du resultat
		assertEquals("Il devrait y avoir le sprinteur rouge sur la case droite de la cellule", s, c.getCaseDroite().getCycliste());
		assertEquals("Il devrait y avoir le sprinteur bleu sur la case gauche de la cellule", r, c.getCaseGauche().getCycliste());
	}
	
	
	@Test
	/**
	 * Test de la methode estLibreCellule quand la cellule est vide 
	 */
	void testEstLibreCellule1() {
		//Preparation des donnees
		Cellule c = new Cellule(0);
		//Validation du resultat
		assertEquals("La cellule devrait etre libre", true, c.estLibreCellule());
	}
	
	@Test
	/**
	 * Test de la methode estLibreCellule quand la cellule est occupee a droite
	 */
	void testEstLibreCellule2() {
		//Preparation des donnees
		Cellule c = new Cellule(0);
		Sprinteur s = new Sprinteur ("R");
		c.allerSurCellule(s);
		//Validation du resultat
		assertEquals("La cellule devrait etre libre", true, c.estLibreCellule());
	}
	
	@Test
	/**
	 * Test de la methode estLibreCellule quand la cellule est occupee a droite et a gauche
	 */
	void testEstLibreCellule3() {
		//Preparation des donnees
		Cellule c = new Cellule(0);
		Sprinteur s = new Sprinteur ("R");
		Rouleur r = new Rouleur("B");
		c.allerSurCellule(s);
		c.allerSurCellule(r);
		//Validation du resultat
		assertEquals("La cellule ne devrait pas etre libre", false, c.estLibreCellule());
	}
	
	@Test
	/**
	 * Test de la methode celluleOccupeDroite quand la cellule est vide 
	 */
	void testCelluleOccupeDroite1(){
		//Preparation des donnees
		Cellule c = new Cellule(0);
		//Validation du resultat
		assertEquals("La cellule ne devrait pas etre occupee a droite", false, c.celluleOccupeDroite());
	}
	
	@Test
	/**
	 * Test de la methode celluleOccupeDroite quand la cellule est occupee a droite
	 */
	void testCelluleOccupeDroite2(){
		//Preparation des donnees
		Cellule c = new Cellule(0);
		Sprinteur s = new Sprinteur ("R");
		c.allerSurCellule(s);
		//Validation du resultat
		assertEquals("La cellule devrait etre occupee a droite", true, c.celluleOccupeDroite());
	}


	@Test
	/**
	 * Test de la methode celluleOccupeGauche quand la cellule est vide 
	 */
	void testCelluleOccupeGauche1(){
		//Preparation des donnees
		Cellule c = new Cellule(0);
		//Validation du resultat
		assertEquals("La cellule ne devrait pas etre occupee a gauche", false, c.celluleOccupeGauche());
	}
	
	@Test
	/**
	 * Test de la methode celluleOccupeGauche quand la cellule est occupee a droite
	 */
	void testCelluleOccupeGauche2(){
		//Preparation des donnees
		Cellule c = new Cellule(0);
		Sprinteur s = new Sprinteur ("R");
		c.allerSurCellule(s);
		//Validation du resultat
		assertEquals("La cellule ne devrait pas etre occupee a gauche", false, c.celluleOccupeGauche());
	}
	
	@Test
	/**
	 * Test de la methode celluleOccupeGauche quand la cellule est occupee a gauche
	 */
	void testCelluleOccupeGauche3(){
		//Preparation des donees
		Cellule c = new Cellule(0);
		Sprinteur s = new Sprinteur ("R");
		c.allerSurCellule(s);
		Rouleur r = new Rouleur("B");
		c.allerSurCellule(r);
		//Validation du resultat
		assertEquals("La cellule  devrait etre occupee a gauche", true, c.celluleOccupeGauche());
	}
	
	@Test
	/**
	 * Test de la methode viderCellule quand il y a un cycliste a droite 
	 */
	void testViderCellule1(){
		//Preparation des donnees
		Cellule c = new Cellule(0);
		Sprinteur s = new Sprinteur ("R");
		c.allerSurCellule(s);
		//Methode testee
		c.viderCellule(s);
		//Validation du resultat
		assertEquals("La case droite de la cellule devrait etre nulle", null, c.getCaseDroite().getCycliste());
	}
	
	@Test
	/**
	 * Test de la methode viderCellule quand il y a un cycliste a droite et a gauche et qu'on enleve le cycliste de droite
	 */
	void testViderCellule2(){
		//Preparation des donnees
		Cellule c = new Cellule(0);
		Sprinteur s = new Sprinteur ("R");
		c.allerSurCellule(s);
		Rouleur r = new Rouleur("B");
		c.allerSurCellule(r);
		//Methode testee
		c.viderCellule(s);
		//Validation du resultat
		assertEquals("La case droite de la cellule devrait etre nulle", null, c.getCaseDroite().getCycliste());
		assertEquals("La case gauche de la cellule devrait etre remplie", r , c.getCaseGauche().getCycliste());
	}
	
	@Test
	/**
	 * Test de la methode viderCellule quand il y a un cycliste a droite et a gauche et qu'on enleve le cycliste de gauche  
	 */
	void testViderCellule3(){
		//Preparation des donnees
		Cellule c = new Cellule(0);
		Sprinteur s = new Sprinteur ("R");
		c.allerSurCellule(s);
		Rouleur r = new Rouleur("B");
		c.allerSurCellule(r);
		//Methode testee
		c.viderCellule(r);
		//Validation du resultat
		assertEquals("La case droite de la cellule devrait etre remplie", s, c.getCaseDroite().getCycliste());
		assertEquals("La case gauche de la cellule devrait etre nulle", null, c.getCaseGauche().getCycliste());
	}
	
	@Test
	/**
	 * Test de la methode decallerJoueur quand il y a un cycliste a droite et a gauche
	 */
	void testDecallerJoueur1() {
		//Preparation des donnees
		Cellule c = new Cellule(0);
		Sprinteur s = new Sprinteur ("R");
		c.allerSurCellule(s);
		Rouleur r = new Rouleur("B");
		c.allerSurCellule(r);
		//Methode testee
		c.decallerJoueur();
		//Validation du resultat
		assertEquals("La case droite de la cellule devrait etre remplie", s, c.getCaseDroite().getCycliste());
		assertEquals("La case gauche de la cellule devrait etre remplie", r, c.getCaseGauche().getCycliste());
	}
	
	@Test
	/**
	 * Test de la methode decallerJoueur quand il y a un cycliste a gauche
	 */
	void testDecallerJoueur2() {
		//Preparation des donnees
		Cellule c = new Cellule(0);
		Sprinteur s = new Sprinteur ("R");
		c.allerSurCellule(s);
		Rouleur r = new Rouleur("B");
		c.allerSurCellule(r);
		c.viderCellule(s);
		//Methode testee
		c.decallerJoueur();
		//Validation du resultat
		assertEquals("La case droite de la cellule devrait etre remplie", r, c.getCaseDroite().getCycliste());
		assertEquals("La case gauche de la cellule devrait etre nulle", null, c.getCaseGauche().getCycliste());
	}
	
	@Test
	/**
	 * Test de la methode decallerJoueur quand il y a un cycliste a droite
	 */
	void testDecallerJoueur3() {
		//Preparation des donnees
		Cellule c = new Cellule(0);
		Sprinteur s = new Sprinteur ("R");
		c.allerSurCellule(s);
		//Methode testee
		c.decallerJoueur();
		//Validation du resultat
		assertEquals("Le cycliste ne devrait pas avoir bouge", s, c.getCaseDroite().getCycliste());
	}
	
	
	

}
