/**
 * Classe Carte Fatigue qui extends la classe Carte
 * 
 */
public class CarteFatigue extends Carte {
	/**
	 * Constructeur de la classe CarteFatigue sans parametre
	 * 
	 */
	public CarteFatigue() {
		super(2);
	}
}
