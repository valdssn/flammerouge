import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CaseTest {

	@Test
	/**
	 * Test de la methode estVide quand on cree une nouvelle case 
	 */
	void testEstVide1() {
		//Preparation des donnees
		Case c = new Case();
		//Validation du resultat
		assertEquals("La case devrait etre vide",true,  c.estVide());
	}
	
	@Test
	/**
	 * Test de la methode estVide quand on ajoute un cycliste sur une case 
	 */
	void testEstVide2() {
		//Preparation des donnees
		Case c = new Case();
		Rouleur cy = new Rouleur("R");
		c.setCycliste(cy);
		//Validation du resultat
		assertEquals("La case devrait etre remplie", false,  c.estVide());
	}
	
	@Test
	/**
	 * Test de la methode viderCase
	 */
	void testViderCase() {
		//Preparation des donnees
		Case c = new Case();
		Rouleur cy = new Rouleur("R");
		c.setCycliste(cy);
		//Methode testee
		c.viderCase();
		//Validatin du resultat
		assertEquals("La case devrait etre vide", null,  c.getCycliste());
	}
	
	
	

}
