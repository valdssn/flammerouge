import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarteFatigueTest {
	
	@Test
	/**
	 * test du constructeur CarteFatigue
	 */
	void test_constructeur_CarteFatigue() {
		//Preparation des donnees
		CarteFatigue carte=new CarteFatigue();
		//Validation du resultat
		assertEquals("la carte devrait etre egal a 2", 2, carte.getNumero());
	}

}
