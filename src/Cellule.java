
/**
 * classe Cellule
 *
 */
public class Cellule {

	/**
	 * attributs de type Case Cases droite et gauche de la cellule
	 */
	private Case caseDroite, caseGauche;

	/**
	 * attribut de type entier type de la case 0 pour route plate, 1 pour montee, 2
	 * pour descente et 3 pour arrivee
	 */
	private int typecase;

	/**
	 * Constructeur de la classe cellule a un entier
	 * 
	 * @param type
	 *            type de la cellule
	 */

	public Cellule(int type) {
		this.caseDroite = new Case();
		this.caseGauche = new Case();
		this.typecase = type;
	}

	/**
	 * Methode getTypeCase
	 * 
	 * @return le type de la case (0 pour plat, 1 pour montee, 2 pour descente, 3
	 *         pour arrivee)
	 */
	public int getTypeCase() {
		return typecase;
	}

	/**
	 * Methode getCaseDroite
	 * 
	 * @return la case droite de la cellule
	 */
	public Case getCaseDroite() {
		return this.caseDroite;
	}

	/**
	 * Methode getCaseGauche
	 * 
	 * @return la case gauche de la cellule
	 */
	public Case getCaseGauche() {
		return this.caseGauche;
	}

	/**
	 * Methode allerSurCellule qui place un cycliste sur une cellule
	 * 
	 * @param c
	 *            objet de type cycliste
	 */
	public void allerSurCellule(Cycliste c) {
		if (this.caseDroite.estVide()) {
			this.caseDroite.setCycliste(c);
		} else if (this.caseGauche.estVide()) {
			this.caseGauche.setCycliste(c);
		}
	}

	/**
	 * Methode getMax
	 * 
	 * @return le maximum de case qu'un cycliste peut avancer en fonction du type de
	 *         la case.
	 */
	public int getMax() {
		int type = this.typecase;
		int max;
		if (type == 1) {
			max = 5;
		} else {
			max = 9;
		}
		return max;
	}

	/**
	 * Methode getMin
	 * 
	 * @return le minimum de case qu'un cycliste peut avancer en fonction du type de
	 *         la case.
	 */
	public int getMin() {
		int type = this.typecase;
		int min;
		if (type == 2) {
			min = 5;
		} else {
			min = 0;
		}
		return min;
	}

	/**
	 * Methode estLibreCellule
	 * 
	 * @return true si la cellule est libre
	 */
	public boolean estLibreCellule() {
		boolean libre;
		if (this.caseDroite.estVide() || this.caseGauche.estVide()) {
			libre = true;
		} else {
			libre = false;
		}
		return libre;
	}

	/**
	 * Methode celluleOccupeeDroite
	 * 
	 * @return true si la case droite de la cellule est occupee
	 */
	public boolean celluleOccupeDroite() {
		boolean occupe;
		if (this.caseDroite.estVide()) {
			occupe = false;
		} else {
			occupe = true;
		}
		return occupe;
	}

	/**
	 * Methode celluleOccupeeGauche
	 * 
	 * @return true si la case gauche de la cellule est occupee
	 */
	public boolean celluleOccupeGauche() {
		boolean occupe;
		if (this.caseGauche.estVide()) {
			occupe = false;
		} else {
			occupe = true;
		}
		return occupe;
	}

	/**
	 * Methode getCyclisteCelluleDroite
	 * 
	 * @return le cycliste present sur la case droite de la cellule
	 */
	public Cycliste getCyclisteCelluleDroite() {
		return this.caseDroite.getCycliste();
	}

	/**
	 * Methode getCyclisteCelluleGauche
	 * 
	 * @return le cycliste present sur la case gauche de la cellule
	 */
	public Cycliste getCyclisteCelluleGauche() {
		return this.caseGauche.getCycliste();
	}

	/**
	 * Methode viderCellule qui permet d'enlever un cycliste d'une celllule
	 * 
	 * @param c
	 *            objet de type cycliste
	 */
	public void viderCellule(Cycliste c) {
		if (this.caseGauche.getCycliste() == c) {
			this.caseGauche.setCycliste(null);
		} else {
			if (this.caseDroite.getCycliste() == c) {
				this.caseDroite.setCycliste(null);
			}
		}
	}

	/**
	 * Methode decallerJoueur qui permet de decaller le joueur de gauche a droite si
	 * le joueur qui etait a sa droite a avance
	 */
	public void decallerJoueur() {
		if ((!this.celluleOccupeDroite()) && (this.celluleOccupeGauche())) {
			Cycliste c = this.getCaseGauche().getCycliste();
			this.getCaseGauche().setCycliste(null);
			this.getCaseDroite().setCycliste(c);
		}
	}

}
