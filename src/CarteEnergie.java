
/**
 * Classe Carte energie qui extends la classe Carte
 * 
 */
public class CarteEnergie extends Carte {
	/**
	 * Constructeur de la classe CarteEnergie a un entier
	 * 
	 * @param n
	 *            numero de la carte
	 */
	public CarteEnergie(int n) {
		super(n);
	}

}
